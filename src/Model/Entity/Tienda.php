<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tienda Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $description
 * @property string $email
 * @property string $telefono
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $web
 * @property string $url
 * @property string|null $foto_dir
 * @property string|null $foto
 * @property string|null $logo_dir
 * @property string|null $logo
 *  * @property string|null $name
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Tienda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'nombre' => true,
        'descripcion' => true,
        'description' => true,
        'email' => true,
        'telefono' => true,
        'facebook' => true,
        'instagram' => true,
        'twitter' => true,
        'web' => true,
        'foto_dir' => true,
        'foto' => true,
        'foto2_dir' => true,
        'foto2' => true,
        'foto3_dir' => true,
        'foto3' => true,
        'foto4_dir' => true,
        'foto4' => true,
        'foto5_dir' => true,
        'foto5' => true,
        'logo_dir' => true,
        'logo' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'horario' => true,
        'hours' => true,
        'name' => true,
        'url' => true
    ];
}
