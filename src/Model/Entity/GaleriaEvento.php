<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GaleriaEvento Entity
 *
 * @property int $id
 * @property int $evento_id
 * @property string|null $titulo
 * @property string|null $title
 * @property string|null $url
 * @property string|null $foto
 * @property string|null $foto_dir
 * @property string|null $tipo
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Actividade $actividade
 */
class GaleriaEvento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'evento_id' => true,
        'titulo' => true,
        'title' => true,
        'url' => true,
        'foto' => true,
        'foto_dir' => true,
        'foto2' => true,
        'foto2_dir' => true,
        'foto3' => true,
        'foto3_dir' => true,
        'foto4' => true,
        'foto4_dir' => true,
        'foto5' => true,
        'foto5_dir' => true,
        'foto6' => true,
        'foto6_dir' => true,
        'foto7' => true,
        'foto7_dir' => true,
        'foto8' => true,
        'foto8_dir' => true,
        'foto9' => true,
        'foto9_dir' => true,
        'foto10' => true,
        'foto10_dir' => true,
        'foto11' => true,
        'foto11_dir' => true,
        'foto12' => true,
        'foto12_dir' => true,
        'foto13' => true,
        'foto13_dir' => true,
        'foto14' => true,
        'foto14_dir' => true,
        'foto15' => true,
        'foto15_dir' => true,
        'foto16' => true,
        'foto16_dir' => true,
        'foto17' => true,
        'foto17_dir' => true,
        'foto18' => true,
        'foto18_dir' => true,
        'foto19' => true,
        'foto19_dir' => true,
        'foto20' => true,
        'foto20_dir' => true,
        'foto21' => true,
        'foto21_dir' => true,
        'foto22' => true,
        'foto22_dir' => true,
        'foto23' => true,
        'foto23_dir' => true,
        'foto24' => true,
        'foto24_dir' => true,
        'foto25' => true,
        'foto25_dir' => true,
        'foto26' => true,
        'foto26_dir' => true,
        'foto27' => true,
        'foto27_dir' => true,
        'foto28' => true,
        'foto28_dir' => true,
        'foto29' => true,
        'foto29_dir' => true,
        'foto30' => true,
        'foto30_dir' => true,
        'tipo' => true,
        'year' => true,
        'created' => true,
        'modified' => true,
        'actividade' => true
    ];
}
