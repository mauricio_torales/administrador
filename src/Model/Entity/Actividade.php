<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Actividade Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $description
 * @property string|null $logo
 * @property string|null $logo_dir
 * @property string|null $foto
 * @property string|null $foto_dir
 * @property string|null $foto2
 * @property string|null $foto2_dir
 * @property string|null $foto3
 * @property string|null $foto3_dir
 * @property string|null $foto4
 * @property string|null $foto4_dir
 * @property string|null $foto5
 * @property string|null $foto5_dir
 * @property string|null $foto6
 * @property string|null $foto6_dir
 * @property string|null $foto7
 * @property string|null $foto7_dir
 * @property string|null $foto8
 * @property string|null $foto8_dir
 * @property string|null $foto9
 * @property string|null $foto9_dir
 * @property string|null $foto10
 * @property string|null $foto10_dir
 * @property string|null $foto11
 * @property string|null $foto11_dir
 * @property string|null $foto12
 * @property string|null $foto12_dir
 * @property string|null $foto13
 * @property string|null $foto13_dir
 * @property string|null $foto14
 * @property string|null $foto14_dir
 * @property string|null $foto15
 * @property string|null $foto15_dir
 * @property string|null $foto16
 * @property string|null $foto16_dir
 * @property string|null $foto17
 * @property string|null $foto17_dir
 * @property string|null $foto18
 * @property string|null $foto18_dir
 * @property string|null $name
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Actividade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'nombre' => true,
        'descripcion' => true,
        'description' => true,
        'logo' => true,
        'logo_dir' => true,
        'foto' => true,
        'foto_dir' => true,
        'foto2' => true,
        'foto2_dir' => true,
        'foto3' => true,
        'foto3_dir' => true,
        'foto4' => true,
        'foto4_dir' => true,
        'foto5' => true,
        'foto5_dir' => true,
        'foto6' => true,
        'foto6_dir' => true,
        'foto7' => true,
        'foto7_dir' => true,
        'foto8' => true,
        'foto8_dir' => true,
        'foto9' => true,
        'foto9_dir' => true,
        'foto10' => true,
        'foto10_dir' => true,
        'foto11' => true,
        'foto11_dir' => true,
        'foto12' => true,
        'foto12_dir' => true,
        'foto13' => true,
        'foto13_dir' => true,
        'foto14' => true,
        'foto14_dir' => true,
        'foto15' => true,
        'foto15_dir' => true,
        'foto16' => true,
        'foto16_dir' => true,
        'foto17' => true,
        'foto17_dir' => true,
        'foto18' => true,
        'foto18_dir' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'orden' => true,
        'name' => true
    ];
}
