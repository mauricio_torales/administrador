<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Popup Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $foto_dir
 * @property |null $foto
 * @property string|null $activo
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property float|null $dia
 * @property float|null $mes
 * @property float|null $anho
 * @property string|null $video
 * @property float|null $fin_dia
 * @property float|null $fin_mes
 * @property float|null $fin_anho
 *
 * @property \App\Model\Entity\User $user
 */
class Popup extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'foto_dir' => true,
        'nombre' => true,
        'foto' => true,
        'activo' => true,
        'created' => true,
        'modified' => true,
        'dia' => true,
        'mes' => true,
        'anho' => true,
        'video' => true,
        'fin_dia' => true,
        'fin_mes' => true,
        'fin_anho' => true,
        'user' => true
    ];
}
