<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Popups Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Popup get($primaryKey, $options = [])
 * @method \App\Model\Entity\Popup newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Popup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Popup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Popup|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Popup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Popup[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Popup findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PopupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('popups');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('foto_dir')
            ->allowEmptyString('foto_dir');

        $validator
            ->allowEmptyString('foto');

        $validator
            ->scalar('activo')
            ->maxLength('activo', 1)
            ->allowEmptyString('activo');

        $validator
            ->decimal('dia')
            ->allowEmptyString('dia');

        $validator
            ->decimal('mes')
            ->allowEmptyString('mes');

        $validator
            ->decimal('anho')
            ->allowEmptyString('anho');

        $validator
            ->scalar('video')
            ->allowEmptyString('video');

        $validator
            ->decimal('fin_dia')
            ->allowEmptyString('fin_dia');

        $validator
            ->decimal('fin_mes')
            ->allowEmptyString('fin_mes');

        $validator
            ->decimal('fin_anho')
            ->allowEmptyString('fin_anho');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
