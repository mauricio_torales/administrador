<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tienda $tienda
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
    content: "photo_size_select_actual";
    font-family: 'Material Icons';
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
 

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>

<ol class="breadcrumb">
    <li class="breadcrumb-item">Locales</li>
    <li class="breadcrumb-item active">Agregar</li>
</ol>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong>Locales</strong>
            <small>formulario</small>
        </div>

        <?= $this->Form->create($tienda,['type' => 'file']) ?>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Ingrese el nombre del local" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre <span style="color:red;">Ingles</span></label>
                        <input class="form-control" id="name" name="name" type="text" placeholder="Ingrese el nombre del local en ingles" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Descripción</label>
                        <textarea name="descripcion" id="editor"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Descripción <span style="color:red;">Ingles</span></label>
                        <textarea name="description" id="editor2"></textarea>                    
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-clock"></i> Horario</label>
                        <input class="form-control" id="horario" name="horario" type="text" placeholder="Horario de atencion">
                    </div>
                    <div class="form-group">
                        <label for="name">Horario <span style="color:red;">Ingles</span></label>
                        <input class="form-control" id="hours" name="hours" type="text" placeholder="Horario de atencion del local en ingles" required>
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-screen-smartphone"></i> Telefono</label>
                        <input class="form-control" id="telefono" name="telefono" type="text" placeholder="+595 999 999 999">
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-envelope"></i> Email</label>
                        <input class="form-control" id="email" name="email" type="email" placeholder="ejemplo@ejemplo.com">
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-social-facebook"></i> Facebook</label>
                        <input class="form-control" id="facebook" name="facebook" type="text" placeholder="https://www.facebook.com/pagina del local">
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-social-instagram"></i> Instagram</label>
                        <input class="form-control" id="instagram" name="instagram" type="text" placeholder="https://www.instagram.com/pagina del local">
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-social-twitter"></i> Twitter</label>
                        <input class="form-control" id="twitter" name="twitter" type="text" placeholder="https://www.twitter.com/pagina del local">
                    </div>
                    <div class="form-group">
                        <label for="name"><i class="icon-globe"></i> Web</label>
                        <input class="form-control" id="web" name="web" type="text" placeholder="sitio web del local">
                    </div>
                   
                </div>
                <div class="col-sm-6">
                <center><label for="name">Logo</label></center>
                
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                        <label>
                            <input type="file" id="logo" name="logo" class="image-upload" accept="image/*" />
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                <center><label for="name">Portada</label></center>
                
                    <div class="box">
                        <div  id="vistaprevia" class="js--image-preview"></div>
                        <div class="upload-options">
                        <label>
                            <input type="file" name="upload_image" id="upload_image" class="image-upload" accept="image/*" />
                            <input type="hidden" name="foto" id="uploaded_image">
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                <center><label for="name">Portada 2</label></center>
                    <div class="box">
                        <div id="vistaprevia-2" class="js--image-preview"></div>
                        <div class="upload-options">
                        <label class="lbl-foto2">
                            <input type="file" name="upload_image" id="upload_image_2" class="image-upload" accept="image/*" />
                            <input type="hidden"  id="uploaded_image_2">
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                <center><label for="name">Portada 3</label></center>
                    <div class="box">
                        <div id="vistaprevia-3" class="js--image-preview"></div>
                        <div class="upload-options">
                        <label class="lbl-foto3">
                            <input type="file" name="upload_image" id="upload_image_3" class="image-upload" accept="image/*" />
                            <input type="hidden"  id="uploaded_image_3">
                        </label>
                        </div>
                    </div>
                </div>
                
                
                
               
            </div>
            <div class="card">
        <div class="card-header">
            <small>Video</small>
        </div>
        <div class="card-body">
            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Url del video en Youtube. <br>Ej:<span style="color:red;"> https://www.youtube.com/watch?v=IXdNnw99-Ic</span></label>
                        <input class="form-control" id="url" name="url" type="text" placeholder="codigo del video">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Miniatura o vista previa</label><br>
                        <img style="width:200px;" id="vista" src="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Video</label><br>
                        <iframe  id="video" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>
        </div>
        <?= $this->Form->end() ?>
    </div>
     <!-- /Fin del contenido principal -->
     <div id="uploadimageModal" class="modal" role="dialog">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div id="image_demo" style="margin-top:30px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-success crop_image">Cortar y guardar imagen</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="uploadimageModal-2" class="modal" role="dialog">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div id="image_demo-2" style="margin-top:30px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-success crop_image-2">Cortar y guardar imagen</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="uploadimageModal-3" class="modal" role="dialog">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div id="image_demo-3" style="margin-top:30px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-success crop_image-3">Cortar y guardar imagen</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
</div>
<script>
 $('#url').on('change', function(){

var newval = '',
    $this = $(this);

if (newval = $this.val().match(/(\?|&)v=([^&#]+)/)) {

    $this.val(newval.pop());

} else if (newval = $this.val().match(/(\.be\/)+([^\/]+)/)) {

    $this.val(newval.pop());

} else if (newval = $this.val().match(/(\embed\/)+([^\/]+)/)) {

    $this.val(newval.pop().replace('?rel=0',''));

}

});

$('#url').blur(function() {
var url = document.getElementById("url").value;
$('#video').attr('src', 'https://www.youtube.com/embed/'+ url);
$('#vista').attr('src', 'https://img.youtube.com/vi/'+ url+'/1.jpg'); 
});
  $('.lbl-foto2').click(function(){
      $('#foto2').attr('name', 'foto2');
  });
  $('.lbl-foto3').click(function(){
      $('#foto3').attr('name', 'foto3');
  });
  $('.lbl-foto4').click(function(){
      $('#foto4').attr('name', 'foto4');
  });
  $('.lbl-foto5').click(function(){
      $('#foto5').attr('name', 'foto5');
  });
  function initImageUpload(box) {
    let uploadField = box.querySelector('.image-upload');

    uploadField.addEventListener('change', getFile);

    function getFile(e){
      let file = e.currentTarget.files[0];
      checkType(file);
    }
    
    function previewImage(file){
      let thumb = box.querySelector('.js--image-preview'),
          reader = new FileReader();

      reader.onload = function() {
        thumb.style.backgroundImage = 'url(' + reader.result + ')';
      }
      reader.readAsDataURL(file);
      thumb.className += ' js--no-default';
    }

    function checkType(file){
      let imageType = /image.*/;
      if (!file.type.match(imageType)) {
        throw 'Datei ist kein Bild';
      } else if (!file){
        throw 'Kein Bild gewählt';
      } else {
        previewImage(file);
      }
    }
    
  }

  // initialize box-scope
  var boxes = document.querySelectorAll('.box');

  for (let i = 0; i < boxes.length; i++) {
    let box = boxes[i];
    initDropEffect(box);
    initImageUpload(box);
  }



  /// drop-effect
  function initDropEffect(box){
    let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
    
    // get clickable area for drop effect
    area = box.querySelector('.js--image-preview');
    area.addEventListener('click', fireRipple);
    
    function fireRipple(e){
      area = e.currentTarget
      // create drop
      if(!drop){
        drop = document.createElement('span');
        drop.className = 'drop';
        this.appendChild(drop);
      }
      // reset animate class
      drop.className = 'drop';
      
      // calculate dimensions of area (longest side)
      areaWidth = getComputedStyle(this, null).getPropertyValue("width");
      areaHeight = getComputedStyle(this, null).getPropertyValue("height");
      maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

      // set drop dimensions to fill area
      drop.style.width = maxDistance + 'px';
      drop.style.height = maxDistance + 'px';
      
      // calculate dimensions of drop
      dropWidth = getComputedStyle(this, null).getPropertyValue("width");
      dropHeight = getComputedStyle(this, null).getPropertyValue("height");
      
      // calculate relative coordinates of click
      // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
      x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
      y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
      
      // position drop and animate
      drop.style.top = y + 'px';
      drop.style.left = x + 'px';
      drop.className += ' animate';
      e.stopPropagation();
      
    }
  }


  $(document).ready(function() {
            /* ===== Valida la imagen y luego abre modal ====== */
            $('#upload_image').on('change', function() {
                var upload_image = $("#upload_image").val();
                if (upload_image.length == 0) {
                    alert("Debe subir una imagen");
                    $("#upload_image").focus();
                    return false;
                }
                var fileExtension = ['jpeg', 'jpg', 'png','webp'];
                if ($.inArray($("#upload_image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Debe subir una imagen en formato .jpeg .jpg o .png");
                    $("#upload_image").focus();
                    return false;
                }

                var maxfilesize = 8024 * 8024 * 20; // 2 Mb          
                var archivoSize = document.getElementById('upload_image').files[0].size;
                if (archivoSize > maxfilesize) {
                    alert("Error: Tu imagen supera 2 MB");
                    $("#upload_image").focus();
                    return false;
                }

                var reader = new FileReader();
                reader.onload = function(event) {
                    $image_crop.croppie('bind', {
                        url: event.target.result
                    }).then(function() {});
                }
                reader.readAsDataURL(this.files[0]);
                $('#uploadimageModal').modal('show');
            });

            /* =====  Recorta la imagen ===== */
            $image_crop = $('#image_demo').croppie({
                enableExif: true,
                viewport: {
                    width: 1250,
                    height: 500,
                    type: 'square'
                },
                boundary: {
                    width: 1300,
                    height: 550
                }
            });

            /* =====  Envia la imagen a upload con las medidas fijas ===== */
            $('.crop_image').click(function(event) {
                $image_crop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function(response) {
                            $('#uploadimageModal').modal('hide');
                            $('#uploaded_image').attr('name', 'foto');
                            $('#uploaded_image').val(response.trim());
                            $('#vistaprevia').attr('style', 'background-image:url('+response.trim()+')');
                })
            });
              /* ===== Valida la imagen y luego abre modal ====== */
              $('#upload_image_2').on('change', function() {
                var upload_image = $("#upload_image_2").val();
                if (upload_image.length == 0) {
                    alert("Debe subir una imagen");
                    $("#upload_image_2").focus();
                    return false;
                }
                var fileExtension = ['jpeg', 'jpg', 'png','webp'];
                if ($.inArray($("#upload_image_2").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Debe subir una imagen en formato .jpeg .jpg o .png");
                    $("#upload_image_2").focus();
                    return false;
                }

                var maxfilesize = 8024 * 8024 * 20; // 2 Mb          
                var archivoSize = document.getElementById('upload_image_2').files[0].size;
                if (archivoSize > maxfilesize) {
                    alert("Error: Tu imagen supera 2 MB");
                    $("#upload_image_2").focus();
                    return false;
                }

                var reader = new FileReader();
                reader.onload = function(event) {
                    $image_crop_2.croppie('bind', {
                        url: event.target.result
                    }).then(function() {});
                }
                reader.readAsDataURL(this.files[0]);
                $('#uploadimageModal-2').modal('show');
            });

            /* =====  Recorta la imagen ===== */
            $image_crop_2 = $('#image_demo-2').croppie({
                enableExif: true,
                viewport: {
                    width: 1250,
                    height: 500,
                    type: 'square'
                },
                boundary: {
                    width: 1300,
                    height: 550
                }
            });

            /* =====  Envia la imagen a upload con las medidas fijas ===== */
            $('.crop_image-2').click(function(event) {
                $image_crop_2.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function(response) {
                            $('#uploadimageModal-2').modal('hide');
                            $('#uploaded_image_2').attr('name', 'foto2');
                            $('#uploaded_image_2').val(response.trim());
                            $('#vistaprevia-2').attr('style', 'background-image:url('+response.trim()+')');
                })
            });
            /* ===== Valida la imagen y luego abre modal ====== */
            $('#upload_image_3').on('change', function() {
                var upload_image = $("#upload_image_3").val();
                if (upload_image.length == 0) {
                    alert("Debe subir una imagen");
                    $("#upload_image_3").focus();
                    return false;
                }
                var fileExtension = ['jpeg', 'jpg', 'png','webp'];
                if ($.inArray($("#upload_image_3").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Debe subir una imagen en formato .jpeg .jpg o .png");
                    $("#upload_image_3").focus();
                    return false;
                }

                var maxfilesize = 8024 * 8024 * 20; // 2 Mb          
                var archivoSize = document.getElementById('upload_image_3').files[0].size;
                if (archivoSize > maxfilesize) {
                    alert("Error: Tu imagen supera 2 MB");
                    $("#upload_image_3").focus();
                    return false;
                }

                var reader = new FileReader();
                reader.onload = function(event) {
                    $image_crop_3.croppie('bind', {
                        url: event.target.result
                    }).then(function() {});
                }
                reader.readAsDataURL(this.files[0]);
                $('#uploadimageModal-3').modal('show');
            });

            /* =====  Recorta la imagen ===== */
            $image_crop_3 = $('#image_demo-3').croppie({
                enableExif: true,
                viewport: {
                    width: 1250,
                    height: 500,
                    type: 'square'
                },
                boundary: {
                    width: 1300,
                    height: 550
                }
            });

            /* =====  Envia la imagen a upload con las medidas fijas ===== */
            $('.crop_image-3').click(function(event) {
                $image_crop_3.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function(response) {
                        $('#uploadimageModal-3').modal('hide');
                        $('#uploaded_image_3').attr('name', 'foto3');
                        $('#uploaded_image_3').val(response.trim());
                        $('#vistaprevia-3').attr('style', 'background-image:url(../../img/portadas/'+response.trim()+')');
                })
            });
});
</script>