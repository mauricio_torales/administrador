<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tienda $tienda
 */
?>
<style>
.local{
    background-color:white;
}
.portada{
    width:100%;
}
.titulo{
    margin-top:30px;
    margin-left:30px;
}
.descripcion{
    margin-top:30px;
    margin-left:30px;
    width:80%;
}
.logo{
    padding-bottom:50px;
    border-top:1px solid black;
}
.logo h3, .logo img{
    margin-top:20px;
    margin-left:40%;
}
.logo img{
    width:200px;
    height:200px;
}
.contactos{
    margin-left:30px;
}
.contactos i{
    font-size:20px;
}

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Locales</li>
    <li class="breadcrumb-item active"><?= h($tienda->nombre) ?></li>
    
</ol>


<div class="container-fluid">
    <div class="card">
                        <div class="card-header">
                            <?= $this->Html->link(
                                '<i class="icon-plus"></i>' . __('Editar').'</a>',
                                ['controller' => 'Tiendas','action' => 'edit', $tienda->id],
                                ['escape' => false, 'class' => 'btn btn-success']
                            ) ?>
                        </div>
        <div class=" local">
            <?php echo '<img class="portada" src="'.$tienda->foto.'">'; ?>
            <h1 class="titulo" ><?= $tienda->nombre?></h1>
            <h1 class="titulo" ><?= $tienda->name?></h1>

            <p class="descripcion"><?= $tienda->descripcion?></p>
            <p class="descripcion"><?= $tienda->description?></p>
            <p class="contactos"><i class="icon-clock"></i> <?= $tienda->horario?></p>
            <p class="contactos"><i class="icon-envelope"></i> <?= $tienda->email?></p>
            <p class="contactos"><i class="icon-screen-smartphone"></i> <?= $tienda->telefono?></p>
            <p class="contactos"><i class="icon-social-facebook"></i> <?= $tienda->facebook?></p>
            <p class="contactos"><i class="icon-social-instagram"></i> <?= $tienda->instagram?></p>
            <p class="contactos"><i class="icon-social-twitter"></i> <?= $tienda->twitter?></p>
            
            <div class="logo">
                <h3>Logo del Local</h3>
                <?php echo '<img src="http://www.viaallegra.com.py/administrador/'.$tienda->logo_dir.''.$tienda->logo.'">'; ?>
            </div>
        </div>
    </div>
</div>
