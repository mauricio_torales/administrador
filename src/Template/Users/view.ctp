<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Locales'), ['controller' => 'Locales', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Locale'), ['controller' => 'Locales', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= h($user->user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Eventos') ?></h4>
        <?php if (!empty($user->eventos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Nombre') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Foto') ?></th>
                <th scope="col"><?= __('Logo Dir') ?></th>
                <th scope="col"><?= __('Logo') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->eventos as $eventos): ?>
            <tr>
                <td><?= h($eventos->id) ?></td>
                <td><?= h($eventos->user_id) ?></td>
                <td><?= h($eventos->nombre) ?></td>
                <td><?= h($eventos->descripcion) ?></td>
                <td><?= h($eventos->foto_dir) ?></td>
                <td><?= h($eventos->foto) ?></td>
                <td><?= h($eventos->logo_dir) ?></td>
                <td><?= h($eventos->logo) ?></td>
                <td><?= h($eventos->created) ?></td>
                <td><?= h($eventos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Eventos', 'action' => 'view', $eventos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Eventos', 'action' => 'edit', $eventos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Eventos', 'action' => 'delete', $eventos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Locales') ?></h4>
        <?php if (!empty($user->locales)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Nombre') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Telefono') ?></th>
                <th scope="col"><?= __('Facebook') ?></th>
                <th scope="col"><?= __('Instagram') ?></th>
                <th scope="col"><?= __('Twitter') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Foto') ?></th>
                <th scope="col"><?= __('Logo Dir') ?></th>
                <th scope="col"><?= __('Logo') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->locales as $locales): ?>
            <tr>
                <td><?= h($locales->id) ?></td>
                <td><?= h($locales->user_id) ?></td>
                <td><?= h($locales->nombre) ?></td>
                <td><?= h($locales->descripcion) ?></td>
                <td><?= h($locales->email) ?></td>
                <td><?= h($locales->telefono) ?></td>
                <td><?= h($locales->facebook) ?></td>
                <td><?= h($locales->instagram) ?></td>
                <td><?= h($locales->twitter) ?></td>
                <td><?= h($locales->foto_dir) ?></td>
                <td><?= h($locales->foto) ?></td>
                <td><?= h($locales->logo_dir) ?></td>
                <td><?= h($locales->logo) ?></td>
                <td><?= h($locales->created) ?></td>
                <td><?= h($locales->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Locales', 'action' => 'view', $locales->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Locales', 'action' => 'edit', $locales->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Locales', 'action' => 'delete', $locales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locales->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
