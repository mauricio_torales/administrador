<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GaleriaEvento $galeriaEvento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Galeria Evento'), ['action' => 'edit', $galeriaEvento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Galeria Evento'), ['action' => 'delete', $galeriaEvento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $galeriaEvento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Galeria Eventos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Galeria Evento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Actividades'), ['controller' => 'Actividades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Actividade'), ['controller' => 'Actividades', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="galeriaEventos view large-9 medium-8 columns content">
    <h3><?= h($galeriaEvento->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Actividade') ?></th>
            <td><?= $galeriaEvento->has('actividade') ? $this->Html->link($galeriaEvento->actividade->id, ['controller' => 'Actividades', 'action' => 'view', $galeriaEvento->actividade->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($galeriaEvento->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($galeriaEvento->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($galeriaEvento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($galeriaEvento->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($galeriaEvento->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Url') ?></h4>
        <?= $this->Text->autoParagraph(h($galeriaEvento->url)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto') ?></h4>
        <?= $this->Text->autoParagraph(h($galeriaEvento->foto)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($galeriaEvento->foto_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Tipo') ?></h4>
        <?= $this->Text->autoParagraph(h($galeriaEvento->tipo)); ?>
    </div>
</div>
