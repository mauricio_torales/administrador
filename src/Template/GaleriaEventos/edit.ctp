<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GaleriaEvento $galeriaEvento
 */

?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
   
   
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }
  .btn-eliminar{
      background-color:red;
      color:white!important;
      font-weight: bold;
      padding-left:20px!important;
      padding-right: 20px;
      padding-bottom:5px;
      border: 1px solid white;
      cursor:pointer;

  }
  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Album</li>
    <li class="breadcrumb-item active"><?= h($galeriaEvento->titulo) ?></li>
</ol>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong>Album</strong>
            <small>Editar</small>
        </div>  
        <?= $this->Form->create($galeriaEvento,['type' => 'file']) ?>
        <div class="card-body">
            <div class="row">

              <div class="col-sm-12">
                <div class="form-group">
                    <label for="name">Titulo del Album</label>
                    <?php echo $this->Form->control('titulo',['label' => false,'class'=>'form-control']);?>
                </div>
                <div class="form-group">
                    <label for="name">Titulo del Album <span style="color:red;">en Ingles</span></label>
                    <?php echo $this->Form->control('title',['label' => false,'class'=>'form-control']);?>
                </div>
                <div class="form-group">
                    <label for="name">Año</label>
                    <?php echo $this->Form->control('year',['label' => false,'class'=>'form-control']);?>
                </div>
              </div>
            </div>
        </div>
    </div>
        <div class="card">
            <div class="card-header">
                <small>Video</small>
            </div>
            <div class="card-body">
                <div class="row">
                
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name">Url del video en Youtube. <br>Ej:<span style="color:red;"> https://www.youtube.com/watch?v=IXdNnw99-Ic</span></label>
                            <?php echo $this->Form->control('url',['label' => false,'id'=>'url','class'=>'form-control']);?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name">Miniatura o vista previa</label><br>
                            <img style="width:200px;" id="vista" src="https://img.youtube.com/vi/<?=($galeriaEvento->url)?>/1.jpg">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name">Video</label><br>
                            <iframe  id="video" src="https://www.youtube.com/embed/<?=($galeriaEvento->url)?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <small>Imagenes</small>
            </div>
            <div class="card-body">
                <div class="row fila1" >
                    <?php if (!empty($galeriaEvento->foto)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-1" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto_dir.''.$galeriaEvento->foto.');">
                                <button onclick="eliminar(1)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                                
                              <div class="upload-options">
                                <label class="lbl-foto">
                                    <input type="file" id="foto" name="" class="image-upload" accept="image/*" />
                                </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                        <div class="col-sm-4">
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto">
                                        <input type="file" id="foto" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto2)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-2" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto2_dir.''.$galeriaEvento->foto2.');">
                                <button onclick="eliminar(2)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto2">
                                  <input type="file" id="foto2" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                        <div class="col-sm-4 foto2">
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto2">
                                        <input type="file" id="foto2" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto3)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-3" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto3_dir.''.$galeriaEvento->foto3.');">
                                <button onclick="eliminar(3)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto3">
                                  <input type="file" id="foto3" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                    <?php }else{?>
                        <div class="col-sm-4 foto3" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto3">
                                        <input type="file" id="foto3" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila1" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class=" row fila2" style="display:none">
                    <?php if (!empty($galeriaEvento->foto4)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-4" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto4_dir.''.$galeriaEvento->foto4.');">
                                <button onclick="eliminar(4)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>   
                                </div>';
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto4">
                                  <input type="file" id="foto4" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto4" >
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label class="lbl-foto4">
                                            <input type="file" id="foto4" name="" class="image-upload" accept="image/*" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                     <?php }?>
                    <?php if (!empty($galeriaEvento->foto5)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-5" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto5_dir.''.$galeriaEvento->foto5.');">
                                <button onclick="eliminar(5)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto5">
                                  <input type="file" id="foto5" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto5" >
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label class="lbl-foto5">
                                            <input type="file" id="foto5" name="" class="image-upload" accept="image/*" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto6)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-6" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto6_dir.''.$galeriaEvento->foto6.');">
                                <button onclick="eliminar(6)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto6">
                                  <input type="file" id="foto6" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto6" >
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label class="lbl-foto6">
                                            <input type="file" id="foto6" name="" class="image-upload" accept="image/*" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila2" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                    </div>
                 <div class="row fila3" style="display:none">
                    <?php if (!empty($galeriaEvento->foto7)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-7" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto7_dir.''.$galeriaEvento->foto7.');">
                                <button onclick="eliminar(7)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto7">
                                  <input type="file" id="foto7" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto7" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto7">
                                        <input type="file" id="foto7" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto8)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-8" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto8_dir.''.$galeriaEvento->foto8.');">
                                <button onclick="eliminar(8)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto8">
                                  <input type="file" id="foto8" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto8" >
                                <div class="box">
                                    <div class="js--image-preview"></div>
                                    <div class="upload-options">
                                        <label class="lbl-foto8">
                                            <input type="file" id="foto8" name="" class="image-upload" accept="image/*" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto9)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-9" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto9_dir.''.$galeriaEvento->foto9.');">
                                <button onclick="eliminar(9)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto9">
                                  <input type="file" id="foto9" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto9" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto9">
                                        <input type="file" id="foto9" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila3" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                    </div>
                 <div class=" row fila4" style="display:none">
                    <?php if (!empty($galeriaEvento->foto10)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-10" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto10_dir.''.$galeriaEvento->foto10.');">
                                <button onclick="eliminar(10)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto10">
                                  <input type="file" id="foto10" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto10" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto10">
                                        <input type="file" id="foto10" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto11)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                          <?php
                                echo '<div id="preview-11" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto11_dir.''.$galeriaEvento->foto11.');">
                                <button onclick="eliminar(11)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto11">
                                  <input type="file" id="foto11" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto11" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto11">
                                        <input type="file" id="foto11" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto12)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-12" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto12_dir.''.$galeriaEvento->foto12.');">
                                <button onclick="eliminar(12)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto12">
                                  <input type="file" id="foto12" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto12" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto12">
                                        <input type="file" id="foto12" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila4" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila5" style="display:none">
                    <?php if (!empty($galeriaEvento->foto13)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                 <?php
                                echo '<div id="preview-13" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto13_dir.''.$galeriaEvento->foto13.');">
                                <button onclick="eliminar(13)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto13">
                                  <input type="file" id="foto13" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto13" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto13">
                                        <input type="file" id="foto13" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto14)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-14" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto14_dir.''.$galeriaEvento->foto14.');">
                                <button onclick="eliminar(14)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto14">
                                  <input type="file" id="foto14" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto14" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto14">
                                        <input type="file" id="foto14" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto15)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto15_dir.''.$galeriaEvento->foto15.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto15">
                                  <input type="file" id="foto15" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto15" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto15">
                                        <input type="file" id="foto15" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila5" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila6" style="display:none">
                    <?php if (!empty($galeriaEvento->foto16)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto16_dir.''.$galeriaEvento->foto16.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto16">
                                  <input type="file" id="foto16" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto16" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto16">
                                        <input type="file" id="foto16" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto17)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto17_dir.''.$galeriaEvento->foto17.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto17">
                                  <input type="file" id="foto17" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto17" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto17">
                                        <input type="file" id="foto17" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto18)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto18_dir.''.$galeriaEvento->foto18.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto18">
                                  <input type="file" id="foto18" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto18" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto18">
                                        <input type="file" id="foto18" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila6" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila7" style="display:none">
                    <?php if (!empty($galeriaEvento->foto19)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto19_dir.''.$galeriaEvento->foto19.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto19">
                                  <input type="file" id="foto19" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto19" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto19">
                                        <input type="file" id="foto19" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto20)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto20_dir.''.$galeriaEvento->foto20.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto20">
                                  <input type="file" id="foto20" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto20" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto20">
                                        <input type="file" id="foto20" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto21)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto21_dir.''.$galeriaEvento->foto21.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto21">
                                  <input type="file" id="foto21" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto21" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto21">
                                        <input type="file" id="foto21" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila7" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila8" style="display:none">
                    <?php if (!empty($galeriaEvento->foto22)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto22_dir.''.$galeriaEvento->foto22.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto22">
                                  <input type="file" id="foto22" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto22" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto22">
                                        <input type="file" id="foto22" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto23)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto23_dir.''.$galeriaEvento->foto23.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto23">
                                  <input type="file" id="foto23" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto23" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto23">
                                        <input type="file" id="foto23" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto24)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto24_dir.''.$galeriaEvento->foto24.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto24">
                                  <input type="file" id="foto24" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto24" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto24">
                                        <input type="file" id="foto24" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila8" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila9" style="display:none">
                    <?php if (!empty($galeriaEvento->foto25)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto25_dir.''.$galeriaEvento->foto25.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto25">
                                  <input type="file" id="foto25" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto25" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto25">
                                        <input type="file" id="foto25" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto26)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto26_dir.''.$galeriaEvento->foto26.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto26">
                                  <input type="file" id="foto26" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto26" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto26">
                                        <input type="file" id="foto26" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto27)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto27_dir.''.$galeriaEvento->foto27.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto27">
                                  <input type="file" id="foto27" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto27" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto27">
                                        <input type="file" id="foto27" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <button class="btn btn-pill btn-success"id="fila9" type="button"><i class="fa fa-plus"></i>&nbsp;Ver mas</button>
                 </div>
                 <div class="row fila10" style="display:none">
                    <?php if (!empty($galeriaEvento->foto28)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto28_dir.''.$galeriaEvento->foto28.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto28">
                                  <input type="file" id="foto28" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto28" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto28">
                                        <input type="file" id="foto28" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto29)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto29_dir.''.$galeriaEvento->foto29.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto29">
                                  <input type="file" id="foto29" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto29" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto29">
                                        <input type="file" id="foto29" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if (!empty($galeriaEvento->foto30)) {?>
                        <div class="col-sm-4">
                          <div class="box">
                                <?php
                                echo '<div id="preview-15" class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$galeriaEvento->foto30_dir.''.$galeriaEvento->foto30.');">
                                <button onclick="eliminar(15)" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>
                                
                                </div>';
                                
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto30">
                                  <input type="file" id="foto30" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                        </div>
                        <?php }else{?>
                            <div class="col-sm-4 foto30" >
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <label class="lbl-foto30">
                                        <input type="file" id="foto30" name="" class="image-upload" accept="image/*" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                 </div>
                    
            </div>
            <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar?',
        text: "Esta seguro de eliminar esta foto!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                    url: "https://www.viaallegra.com.py/api.php?foto="+ name +"&id=<?=$galeriaEvento->id?>",
                    method: "GET",
                    async: false,
              
            });
            $('#preview-'+name).attr('style', 'background-image:url()');
        }
        });
    };
</script>
<script>
    $('#url').on('change', function(){

        var newval = '',
            $this = $(this);

        if (newval = $this.val().match(/(\?|&)v=([^&#]+)/)) {

            $this.val(newval.pop());

        } else if (newval = $this.val().match(/(\.be\/)+([^\/]+)/)) {

            $this.val(newval.pop());

        } else if (newval = $this.val().match(/(\embed\/)+([^\/]+)/)) {

            $this.val(newval.pop().replace('?rel=0',''));

        }

        });
    $('#fila1').click(function(){
        $( ".fila2" ).show();
        $( "#fila1" ).hide();

    });
    $('#fila2').click(function(){
        $( ".fila3" ).show();
        $( "#fila2" ).hide();

    });
    $('#fila3').click(function(){
        $( ".fila4" ).show();
        $( "#fila3" ).hide();

    });
    $('#fila4').click(function(){
        $( ".fila5" ).show();
        $( "#fila4" ).hide();

    });
    $('#fila5').click(function(){
        $( ".fila6" ).show();
        $( "#fila5" ).hide();

    });
    $('#fila6').click(function(){
        $( ".fila7" ).show();
        $( "#fila6" ).hide();

    });
    $('#fila7').click(function(){
        $( ".fila8" ).show();
        $( "#fila7" ).hide();

    });
    $('#fila8').click(function(){
        $( ".fila9" ).show();
        $( "#fila8" ).hide();

    });
    $('#fila9').click(function(){
        $( ".fila10" ).show();
        $( "#fila9" ).hide();

    });

      $('.lbl-foto').click(function(){
        $('#foto').attr('name', 'foto');
        $( ".foto2" ).show();

    });
    $('#url').blur(function() {
        var url = document.getElementById("url").value;
        $('#video').attr('src', 'https://www.youtube.com/embed/'+ url);
        $('#vista').attr('src', 'https://img.youtube.com/vi/'+ url+'/1.jpg'); 
    });

    $('.lbl-foto').click(function(){
        $('#foto').attr('name', 'foto');
        $( ".foto2" ).show();

    });
    $('.lbl-foto2').click(function(){
        $('#foto2').attr('name', 'foto2');
        $( ".foto3" ).show();

    });

    $('.lbl-foto3').click(function(){
        $('#foto3').attr('name', 'foto3');
        $( ".foto4" ).show();
    });
    $('.lbl-foto4').click(function(){
        $('#foto4').attr('name', 'foto4');
        $( ".foto5" ).show();
    });
    $('.lbl-foto5').click(function(){
        $('#foto5').attr('name', 'foto5');
        $( ".foto6" ).show();
    });
    $('.lbl-foto6').click(function(){
        $('#foto6').attr('name', 'foto6');
        $( ".foto7" ).show();
    });
    $('.lbl-foto7').click(function(){
        $('#foto7').attr('name', 'foto7');
        $( ".foto8" ).show();
    });
    $('.lbl-foto8').click(function(){
        $('#foto8').attr('name', 'foto8');
        $( ".foto9" ).show();
    });
    $('.lbl-foto9').click(function(){
        $('#foto9').attr('name', 'foto9');
        $( ".foto10" ).show();
    });
    $('.lbl-foto10').click(function(){
        $('#foto10').attr('name', 'foto10');
        $( ".foto11" ).show();
    });
    $('.lbl-foto11').click(function(){
        $('#foto11').attr('name', 'foto11');
        $( ".foto12" ).show();
    });
    $('.lbl-foto12').click(function(){
        $('#foto12').attr('name', 'foto12');
        $( ".foto13" ).show();
    });
    $('.lbl-foto13').click(function(){
        $('#foto13').attr('name', 'foto13');
        $( ".foto14" ).show();
    });
    $('.lbl-foto14').click(function(){
        $('#foto14').attr('name', 'foto14');
        $( ".foto15" ).show();
    });
    $('.lbl-foto15').click(function(){
        $('#foto15').attr('name', 'foto15');
    });

    $('#eliminar-1').click(function(){
        $('#eliminar-foto-1').attr('name', 'foto');

    });
    function initImageUpload(box) {
        let uploadField = box.querySelector('.image-upload');

        uploadField.addEventListener('change', getFile);

        function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
        }
        
        function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
            thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
        }

        function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
            throw 'Datei ist kein Bild';
        } else if (!file){
            throw 'Kein Bild gewählt';
        } else {
            previewImage(file);
        }
        }
        
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
        let box = boxes[i];
        initDropEffect(box);
        initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
        let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
        
        // get clickable area for drop effect
        area = box.querySelector('.js--image-preview');
        area.addEventListener('click', fireRipple);
        
        function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
            drop = document.createElement('span');
            drop.className = 'drop';
            this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
        }
    }
</script>

