<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Via Allegra';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?= $cakeDescription ?>
       
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('font-awesome.min.css') ?>
    <?= $this->Html->css('simple-line-icons.min.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown" >
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" >
                    <span class="d-md-down-none" >
                   <?php echo $this->Session->read('Auth.User.user');?>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Cuenta</strong>
                    </div>
                    <?= $this->Html->link(
                        '<i class="fa fa-lock"></i>' . __('Cerrar Sesion'),
                        ['controller' => 'Users','action' => 'logout',],
                        ['escape' => false, 'class' => 'dropdown-item']
                    ) ?>
                </div>
            </li>
            <li class="nav-item dropdown" >
            </li>
        </ul>
    </header>

    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="../"><i class="icon-speedometer"></i> Home</a>
                    </li>

                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="icon-bag"></i>' . __('Locales').'</a>',
                            ['controller' => 'Tiendas','action' => 'index',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="icon-event"></i>' . __('Eventos').'</a>',
                            ['controller' => 'Actividades','action' => 'index',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="icon-camera"></i>' . __('Galeria').'</a>',
                            ['controller' => 'Galeria','action' => 'index',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                    <!--
                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="icon-picture"></i>' . __('Banners').'</a>',
                            ['controller' => 'Banners','action' => 'index',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                    -->
                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="icon-bell"></i>' . __('Avisos o Pop Up').'</a>',
                            ['controller' => 'Popups','action' => 'index',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                    
                
                    <li class="nav-item">
                        <?= $this->Html->link(
                            '<i class="fa fa-lock"></i>' . __('Cerrar Sesion').'</a>',
                            ['controller' => 'Users','action' => 'logout',],
                            ['escape' => false, 'class' => 'nav-link']
                        ) ?>
                    </li>
                </ul>
            </nav>
            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
        </div>

        <!-- Contenido Principal -->
        <main class="main">
            <!-- Breadcrumb -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Inicio</li>
            </ol>
           
        </main>
        <!-- /Fin del contenido principal -->
    </div>

    

    <footer class="app-footer">
        <span class="ml-auto">Desarrollado por <a href="https://linco.com.py/" target="_blank" >Linco</a></span>
    </footer>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('popper.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('pace.min.js') ?>
    <?= $this->Html->script('Chart.min.js') ?>
    <?= $this->Html->script('template.js') ?>
   
</body>

</html>