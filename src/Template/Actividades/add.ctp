<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Actividade $actividade
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
    content: "photo_size_select_actual";
    font-family: 'Material Icons';
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Eventos</li>
    <li class="breadcrumb-item active">Agregar</li>
</ol>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong>Eventos</strong>
            <small>formulario</small>
        </div>
        <?= $this->Form->create($actividade,['type' => 'file']) ?>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                <div class="form-group">
                        <label for="name">Orden</label>
                        <input class="form-control" id="orden" name="orden" type="text" placeholder="Orden en el que sera mostrado el evento" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Ingrese el nombre del evento" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre <span style="color:red;">Ingles</span></label>
                        <input class="form-control" id="name" name="name" type="text" placeholder="Ingrese el nombre del evento en ingles" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Descripción</label>
                        <textarea name="descripcion" id="editor"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Descripción <span style="color:red;">Ingles</span></label>
                        <textarea name="description" id="editor2"></textarea>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <center><label for="name">Vista previa</label></center>
                          <div class="box">
                              <div class="js--image-preview"></div>
                              <div class="upload-options">
                              <label>
                                  <input type="file" id="logo" name="logo" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                      </div>
                    </div>
      

                </div>
                <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>
$('.lbl-foto2').click(function(){
    $('#foto2').attr('name', 'foto2');
});
$('.lbl-foto3').click(function(){
    $('#foto3').attr('name', 'foto3');
});
$('.lbl-foto4').click(function(){
    $('#foto4').attr('name', 'foto4');
});
$('.lbl-foto5').click(function(){
    $('#foto5').attr('name', 'foto5');
});
$('.lbl-foto6').click(function(){
    $('#foto6').attr('name', 'foto6');
});
$('.lbl-foto7').click(function(){
    $('#foto7').attr('name', 'foto7');
});
$('.lbl-foto8').click(function(){
    $('#foto8').attr('name', 'foto8');
});
$('.lbl-foto9').click(function(){
    $('#foto9').attr('name', 'foto9');
});
$('.lbl-foto10').click(function(){
    $('#foto10').attr('name', 'foto10');
});
$('.lbl-foto11').click(function(){
    $('#foto11').attr('name', 'foto11');
});
$('.lbl-foto12').click(function(){
    $('#foto12').attr('name', 'foto12');
});
$('.lbl-foto13').click(function(){
    $('#foto13').attr('name', 'foto13');
});
$('.lbl-foto14').click(function(){
    $('#foto14').attr('name', 'foto14');
});
$('.lbl-foto15').click(function(){
    $('#foto15').attr('name', 'foto15');
});
$('.lbl-foto16').click(function(){
    $('#foto16').attr('name', 'foto16');
});
$('.lbl-foto17').click(function(){
    $('#foto17').attr('name', 'foto17');
});
$('.lbl-foto18').click(function(){
    $('#foto18').attr('name', 'foto18');
});

  function initImageUpload(box) {
    let uploadField = box.querySelector('.image-upload');

    uploadField.addEventListener('change', getFile);

    function getFile(e){
      let file = e.currentTarget.files[0];
      checkType(file);
    }
    
    function previewImage(file){
      let thumb = box.querySelector('.js--image-preview'),
          reader = new FileReader();

      reader.onload = function() {
        thumb.style.backgroundImage = 'url(' + reader.result + ')';
      }
      reader.readAsDataURL(file);
      thumb.className += ' js--no-default';
    }

    function checkType(file){
      let imageType = /image.*/;
      if (!file.type.match(imageType)) {
        throw 'Datei ist kein Bild';
      } else if (!file){
        throw 'Kein Bild gewählt';
      } else {
        previewImage(file);
      }
    }
    
  }

  // initialize box-scope
  var boxes = document.querySelectorAll('.box');

  for (let i = 0; i < boxes.length; i++) {
    let box = boxes[i];
    initDropEffect(box);
    initImageUpload(box);
  }



  /// drop-effect
  function initDropEffect(box){
    let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
    
    // get clickable area for drop effect
    area = box.querySelector('.js--image-preview');
    area.addEventListener('click', fireRipple);
    
    function fireRipple(e){
      area = e.currentTarget
      // create drop
      if(!drop){
        drop = document.createElement('span');
        drop.className = 'drop';
        this.appendChild(drop);
      }
      // reset animate class
      drop.className = 'drop';
      
      // calculate dimensions of area (longest side)
      areaWidth = getComputedStyle(this, null).getPropertyValue("width");
      areaHeight = getComputedStyle(this, null).getPropertyValue("height");
      maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

      // set drop dimensions to fill area
      drop.style.width = maxDistance + 'px';
      drop.style.height = maxDistance + 'px';
      
      // calculate dimensions of drop
      dropWidth = getComputedStyle(this, null).getPropertyValue("width");
      dropHeight = getComputedStyle(this, null).getPropertyValue("height");
      
      // calculate relative coordinates of click
      // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
      x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
      y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
      
      // position drop and animate
      drop.style.top = y + 'px';
      drop.style.left = x + 'px';
      drop.className += ' animate';
      e.stopPropagation();
      
    }
  }
</script>