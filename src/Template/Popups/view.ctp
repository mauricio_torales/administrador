<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Popup $popup
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Popup'), ['action' => 'edit', $popup->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Popup'), ['action' => 'delete', $popup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $popup->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Popups'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Popup'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="popups view large-9 medium-8 columns content">
    <h3><?= h($popup->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $popup->has('user') ? $this->Html->link($popup->user->id, ['controller' => 'Users', 'action' => 'view', $popup->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Foto') ?></th>
            <td><?= h($popup->foto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activo') ?></th>
            <td><?= h($popup->activo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($popup->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dia') ?></th>
            <td><?= $this->Number->format($popup->dia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mes') ?></th>
            <td><?= $this->Number->format($popup->mes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Anho') ?></th>
            <td><?= $this->Number->format($popup->anho) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fin Dia') ?></th>
            <td><?= $this->Number->format($popup->fin_dia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fin Mes') ?></th>
            <td><?= $this->Number->format($popup->fin_mes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fin Anho') ?></th>
            <td><?= $this->Number->format($popup->fin_anho) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($popup->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($popup->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Foto Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($popup->foto_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Video') ?></h4>
        <?= $this->Text->autoParagraph(h($popup->video)); ?>
    </div>
</div>
