<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Popup[]|\Cake\Collection\CollectionInterface $popups
 */
?>
<style>
.eliminar-td{
    display:none;
}
</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Pop Up's</li>
    <li class="breadcrumb-item active">Listado</li>
</ol>
<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Notificaciones o Pop up's
                        <?= $this->Html->link(
                            '<i class="icon-plus"></i>' . __('Agregar').'</a>',
                            ['controller' => 'Popups','action' => 'add',],
                            ['escape' => false, 'class' => 'btn btn-success']
                        ) ?>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th>Nombre</th>
                                <th>Inicia</th>
                                <th>Termina</th>
                                <th scope="col" class="actions"><?= __('Eliminar') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($popups as $popup): ?>
                                <tr>
                                    <td><?= $this->Number->format($popup->id) ?></td>
                                    <td><?= h($popup->nombre) ?></td>
                                    <td><?= h($popup->dia) ?> / <?= h($popup->mes) ?> / <?= h($popup->anho) ?> </td>
                                    <td><?= h($popup->fin_dia) ?> / <?= h($popup->fin_mes) ?> / <?= h($popup->fin_anho) ?> </td>
                                    <td class="actions" style="padding:10px;">
                                    <?= $this->Html->link(__('<i class="icon-note"></i> ' .'Editar'), ['action' => 'edit', $popup->id],['escape' => false]) ?> &nbsp;&nbsp;
                                        <?php
                                         $foto = $popup->id;
                                         echo '<button onclick="eliminar('.$foto.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <?= $this->Paginator->prev(__('Ant'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item active">
                                <?= $this->Paginator->first( __('Inicio'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->numbers(['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->next(__('Sig'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->last(__('Final'),['class'=>'page-link']) ?>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Fin ejemplo de tabla Listado -->
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar?',
        text: "Esta seguro de eliminar este pop up!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="popups/delete/"+ name;
        }
        });
    };
</script>