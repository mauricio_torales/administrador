<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Popup $popup
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
   
   
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Notificacion</li>
    <li class="breadcrumb-item active"><?= h($popup->nombre) ?></li>
</ol>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <small>Editar</small>
        </div>
        <?= $this->Form->create($popup,['type' => 'file']) ?>
        <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label for="name">Nombre</label>
                    <?php echo $this->Form->control('nombre',['label' => false,'class'=>'form-control']);?>
                  </div>
                  <div class="form-group">
                    <label for="name">Youtube</label>
                    <?php echo $this->Form->control('video',['label' => false,'class'=>'form-control']);?>
                  </div>
                  <div class="row" >
                      <div class="col-sm-12">    
                          <center><label for="name">Imagen</label></center>
                          <div class="box">
                                <?php 
                                $foto_old= $popup->foto;
                                $foto_dir_old= $popup->foto_dir;
                                echo '<div class="js--image-preview" style="background-image:url(http://www.viaallegra.com.py/administrador/'.$foto_dir_old.''.$foto_old.');"></div>';
                                ?>
                              <div class="upload-options">
                              <label class="lbl-foto">
                                  <input type="file" id="foto" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                      </div> 
                  </div>
                  <div class="form-group">
						<label class="col-md-6 control-label">Inicia</label>
						<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
                                    Día
									<select name="dia" id="dia" >
                                        <?php $dia= $popup->dia; echo  '<option value="'.$dia.'">'.$dia.'</option>';?>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>						  
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
										    <option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
									</select>
									Mes
									<select name="mes" id="mes" >
                                        <?php $mes= $popup->mes; echo  '<option value="'.$mes.'">'.$mes.'</option>';?>
										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>
									</select>
								    Año
									<select name="anho" id="anho" >
                                        <?php $año= $popup->anho; echo  '<option value="'.$año.'">'.$año.'</option>';?>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2029">2029</option>
										<option value="2030">2030</option>
									</select>
                                </div>
						</div>
				  </div>
                  <div class="form-group">
						<label class="col-md-6 control-label">Termina</label>
						<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
                                    Día
									<select name="fin_dia" id="fin_dia" >
                                        <?php $dia= $popup->fin_dia; echo  '<option value="'.$dia.'">'.$dia.'</option>';?>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>						  
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
										    <option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
									</select>
									Mes
									<select name="fin_mes" id="fin_mes" >
                                        <?php $mes= $popup->fin_mes; echo  '<option value="'.$mes.'">'.$mes.'</option>';?>
										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>
									</select>
								    Año
									<select name="fin_anho" id="fin_anho" >
                                        <?php $año= $popup->fin_anho; echo  '<option value="'.$año.'">'.$año.'</option>';?>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2029">2029</option>
										<option value="2030">2030</option>
									</select>
                                </div>
						</div>
				  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label" >Estado</label>
                        <div class="col-sm-9">
                            <select name="activo" id="activo" >
                                <?php $activo = $popup->activo;
                                    if ($activo==1) {
                                      echo '<option value="1">Activo</option>
                                            <option value="0">Inactivo</option>';
                                    }else{
                                      echo '<option value="0">Inactivo</option>
                                            <option value="1">Activo</option>';
                                    }?>					  
                            </select>
                        </div>
                  </div>
              </div>
              <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>

            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
  	$('.jqte-test').jqte();
	
	// settings of status
	var jqteStatus = true;
	$(".status").click(function()
	{
		jqteStatus = jqteStatus ? false : true;
		$('.jqte-test').jqte({"status" : jqteStatus})
	});

  $('.lbl-foto').click(function(){
      $('#foto').attr('name', 'foto');
  });
  
    function initImageUpload(box) {
      let uploadField = box.querySelector('.image-upload');

      uploadField.addEventListener('change', getFile);

      function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
      }
      
      function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
          thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
      }

      function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
          throw 'Datei ist kein Bild';
        } else if (!file){
          throw 'Kein Bild gewählt';
        } else {
          previewImage(file);
        }
      }
      
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
      let box = boxes[i];
      initDropEffect(box);
      initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
      let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
      
      // get clickable area for drop effect
      area = box.querySelector('.js--image-preview');
      area.addEventListener('click', fireRipple);
      
      function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
          drop = document.createElement('span');
          drop.className = 'drop';
          this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
      }
    }
</script>