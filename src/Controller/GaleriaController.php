<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Galeria Controller
 *
 * @property \App\Model\Table\GaleriaTable $Galeria
 *
 * @method \App\Model\Entity\Galerium[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GaleriaController extends AppController
{
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 10,
            'contain' => ['Users']
        ];
        $galeria = $this->paginate($this->Galeria);

        $this->set(compact('galeria'));
    }

    /**
     * View method
     *
     * @param string|null $id Galerium id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $galerium = $this->Galeria->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('galerium', $galerium);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $galerium = $this->Galeria->newEntity();
        if ($this->request->is('post')) {
            $galerium = $this->Galeria->patchEntity($galerium, $this->request->getData());
            $galerium->user_id = $this->Auth->user('id');
            if ($this->Galeria->save($galerium)) {
                $this->Flash->success(__('La imagen se ha guardado en la galeria.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The galerium could not be saved. Please, try again.'));
        }
        $users = $this->Galeria->Users->find('list', ['limit' => 200]);
        $this->set(compact('galerium', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Galerium id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $galerium = $this->Galeria->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $galerium = $this->Galeria->patchEntity($galerium, $this->request->getData());
            $galerium->user_id = $this->Auth->user('id');

            if ($this->Galeria->save($galerium)) {
                $this->Flash->success(__('The galerium has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The galerium could not be saved. Please, try again.'));
        }
        $users = $this->Galeria->Users->find('list', ['limit' => 200]);
        $this->set(compact('galerium', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Galerium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $galerium = $this->Galeria->get($id);
        if ($this->Galeria->delete($galerium)) {
            $this->Flash->success(__('Se ha eliminado la imagen de la galeria.'));
        } else {
            $this->Flash->error(__('The galerium could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
