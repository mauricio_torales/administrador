<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GaleriaEventos Controller
 *
 * @property \App\Model\Table\GaleriaEventosTable $GaleriaEventos
 *
 * @method \App\Model\Entity\GaleriaEvento[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GaleriaEventosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Actividades']
        ];
        $galeriaEventos = $this->paginate($this->GaleriaEventos);

        $this->set(compact('galeriaEventos'));
    }

    /**
     * View method
     *
     * @param string|null $id Galeria Evento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $galeriaEvento = $this->GaleriaEventos->get($id, [
            'contain' => ['Actividades']
        ]);

        $this->set('galeriaEvento', $galeriaEvento);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   unset($this->Customer->validate['foto']);
        $galeriaEvento = $this->GaleriaEventos->newEntity();
        if ($this->request->is('post')) {
            $galeriaEvento = $this->GaleriaEventos->patchEntity($galeriaEvento, $this->request->getData());
            if ($this->GaleriaEventos->save($galeriaEvento)) {
                $this->Flash->success(__('Se ha agregado el album de fotos al evento'));

                return $this->redirect(['controller' => 'Actividades','action' => 'index']); 
            }
            $this->Flash->error(__('The galeria evento could not be saved. Please, try again.'));
        }
        $actividades = $this->GaleriaEventos->Actividades->find('list', ['limit' => 200]);
        $this->set(compact('galeriaEvento', 'actividades'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Galeria Evento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $galeriaEvento = $this->GaleriaEventos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $galeriaEvento = $this->GaleriaEventos->patchEntity($galeriaEvento, $this->request->getData());
            if ($this->GaleriaEventos->save($galeriaEvento)) {
                $this->Flash->success(__('Se ha actualizado el album de fotos.'));

                return $this->redirect(['controller' => 'Actividades','action' => 'index']); 
            }
            $this->Flash->error(__('The galeria evento could not be saved. Please, try again.'));
        }
        $actividades = $this->GaleriaEventos->Actividades->find('list', ['limit' => 200]);
        $this->set(compact('galeriaEvento', 'actividades'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Galeria Evento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $galeriaEvento = $this->GaleriaEventos->get($id);
        if ($this->GaleriaEventos->delete($galeriaEvento)) {
            $this->Flash->success(__('Se ha eliminado el album de fotos.'));
        } else {
            $this->Flash->error(__('The galeria evento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Actividades','action' => 'index']);
    }
}
