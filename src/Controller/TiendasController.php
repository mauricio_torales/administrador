<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Tiendas Controller
 *
 * @property \App\Model\Table\TiendasTable $Tiendas
 *
 * @method \App\Model\Entity\Tienda[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TiendasController extends AppController
{
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['portada']);
        
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $tiendas = $this->paginate($this->Tiendas);

        $this->set(compact('tiendas'));
    }

    /**
     * View method
     *
     * @param string|null $id Tienda id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tienda = $this->Tiendas->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('tienda', $tienda);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tienda = $this->Tiendas->newEntity();
        if ($this->request->is('post')) {
            $tienda = $this->Tiendas->patchEntity($tienda, $this->request->getData());
            $tienda->user_id = $this->Auth->user('id');
            if ($this->Tiendas->save($tienda)) {
                $this->Flash->success(__('Se ha agregado el local con exito'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('no se pudo guardar el local, favor vuelva a intentarlo'));
        }
        $users = $this->Tiendas->Users->find('list', ['limit' => 200]);
        $this->set(compact('tienda', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tienda id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tienda = $this->Tiendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tienda = $this->Tiendas->patchEntity($tienda, $this->request->getData());
            $tienda->user_id = $this->Auth->user('id');
            if ($this->Tiendas->save($tienda)) {
                $this->Flash->success(__('Se ha actualizado el local con exito'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tienda could not be saved. Please, try again.'));
        }
        $users = $this->Tiendas->Users->find('list', ['limit' => 200]);
        $this->set(compact('tienda', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tienda id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $tienda = $this->Tiendas->get($id);
        if ($this->Tiendas->delete($tienda)) {
            $this->Flash->success(__('Se ha eliminado el local'));
        } else {
            $this->Flash->error(__('The tienda could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function portada()
    {
     $this->autoRender = false;
      if(isset($_POST["image"]))
            {
            $data = $_POST["image"];

            $image_array_1 = explode(";", $data);

            $image_array_2 = explode(",", $image_array_1[1]);

            $data = base64_decode($image_array_2[1]);

            $imageName = time() . '.png';

            $ubicacion_imagen = WWW_ROOT . 'img/portadas/' .$imageName;

            file_put_contents($ubicacion_imagen, $data);
            echo $imageName;   
        }
    }
}
