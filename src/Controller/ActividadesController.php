<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Actividades Controller
 *
 * @property \App\Model\Table\ActividadesTable $Actividades
 *
 * @method \App\Model\Entity\Actividade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActividadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $actividades = $this->paginate($this->Actividades);

        $this->set(compact('actividades'));
    }

    /**
     * View method
     *
     * @param string|null $id Actividade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actividade = $this->Actividades->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('actividade', $actividade);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $actividade = $this->Actividades->newEntity();
        if ($this->request->is('post')) {
            $actividade = $this->Actividades->patchEntity($actividade, $this->request->getData());
            $actividade->user_id = $this->Auth->user('id');
            if ($this->Actividades->save($actividade)) {
                $this->Flash->success(__('Se ha guardado el evento, tenga en cuenta que el evento no aparecera hasta que se hayan cargado imagenes a su galeria.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo gardar el evento, favor vuelva a intentarlo.'));
        }
        $users = $this->Actividades->Users->find('list', ['limit' => 200]);
        $this->set(compact('actividade', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Actividade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $actividade = $this->Actividades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $actividade = $this->Actividades->patchEntity($actividade, $this->request->getData());
            $actividade->user_id = $this->Auth->user('id');
            if ($this->Actividades->save($actividade)) {
                $this->Flash->success(__('Evento editado con exito.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo actualizar el evento.'));
        }
        $users = $this->Actividades->Users->find('list', ['limit' => 200]);
        $this->set(compact('actividade', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Actividade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $actividade = $this->Actividades->get($id);

     

        if ($this->Actividades->delete($actividade)) {
            $this->Flash->success(__('Se ha eliminado el evento.'));
        } else {
            $this->Flash->error(__('The actividade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
